<?php

use PHPUnit\Framework\TestCase;
use App\Utils\ConvertisseurBase2;

class ConvertisseurTest extends TestCase{

    //NUMBER TO CHAINE BINAIRE

    public function test_si_nombre_n_est_pas_un_entier_positif(){
        $this -> expectException(InvalidArgumentException::class);
        ConvertisseurBase2::transformNombreToChaineBinaire(-5);
        ConvertisseurBase2::transformNombreToChaineBinaire("coucou");
    }

    public function test_si_nombre_zero_renvoie_chaine_binaire_zero(){
        $this -> assertEquals(ConvertisseurBase2::transformNombreToChaineBinaire(0),"0");
    }

    public function test_si_nombre_un_renvoie_chaine_binaire_un(){
        $this -> assertEquals(ConvertisseurBase2::transformNombreToChaineBinaire(1),"1");
    }

    public function test_si_nombre_renvoie_chaine_binaire(){
        $this -> assertEquals(ConvertisseurBase2::transformNombreToChaineBinaire(2), "10");
        $this -> assertEquals(ConvertisseurBase2::transformNombreToChaineBinaire(5),"101");
        $this -> assertEquals(ConvertisseurBase2::transformNombreToChaineBinaire(27),"11011");
    }


    //CHAINE BINAIRE TO NUMBER

    public function test_si_chaine_est_une_chaine_de_caracteres(){
        $this -> expectException(InvalidArgumentException::class);
        ConvertisseurBase2::transformChaineBinaireToNombre(10101);
    }

    public function test_si_chaine_est_une_chaine_contenant_des_0_ou_des_1(){
        $this -> expectException(InvalidArgumentException::class);
        ConvertisseurBase2::transformChaineBinaireToNombre("coucou");
        ConvertisseurBase2::transformChaineBinaireToNombre("21380");
    }

    public function test_si_chaine_binaire_0_renvoie_nombre_0(){
        $this -> assertEquals(ConvertisseurBase2::transformChaineBinaireToNombre("0"),0);
    }

    public function test_si_chaine_binaire_1_renvoie_nombre_1(){
        $this -> assertEquals(ConvertisseurBase2::transformChaineBinaireToNombre("1"),1);
    }

    public function test_si_chaine_binaire_renvoie_nombre(){
        $this -> assertEquals(ConvertisseurBase2::transformChaineBinaireToNombre("10"), 2);
        $this -> assertEquals(ConvertisseurBase2::transformChaineBinaireToNombre("101"),5);
        $this -> assertEquals(ConvertisseurBase2::transformChaineBinaireToNombre("11011"),27);
    }
    
}