<?php

use PHPUnit\Framework\TestCase;
use App\Utils\ConvertisseurBaseX;

class ConvertisseurBaseXTest extends TestCase{

    //NUMBER TO CHAINE BASE X

    public function test_si_nombre_est_bien_un_entier_positif(){
        $this -> expectException(InvalidArgumentException::class);
        ConvertisseurBaseX::transformNombreToChaineBaseX(-5,2);
        ConvertisseurBaseX::transformNombreToChaineBaseX("coucou",5);
    }

    public function test_si_nombre_renvoie_chaine_sous_forme_de_baseX(){
        $this -> assertEquals(ConvertisseurBaseX::transformNombreToChaineBaseX(0,5),"0");
        $this -> assertEquals(ConvertisseurBaseX::transformNombreToChaineBaseX(9,5),"14");
        $this -> assertEquals(ConvertisseurBaseX::transformNombreToChaineBaseX(10,8), "12");
        $this -> assertEquals(ConvertisseurBaseX::transformNombreToChaineBaseX(56,12),"48");
        $this -> assertEquals(ConvertisseurBaseX::transformNombreToChaineBaseX(2345,10),"2345");
    }


    //CHAINE BASE X TO NUMBER

    public function test_si_chaine_est_une_chaine_de_caracteres_sinon_leve_erreur(){
        $this -> expectException(InvalidArgumentException::class);
        ConvertisseurBaseX::transformChaineBaseXToNombre(10101,10);
    }

     public function test_si_chaine_n_est_pas_une_chaine_de_nombres_contenant_des_nombres_de_0_à_x_moins_1_leve_exception(){
        $this -> expectException(InvalidArgumentException::class);
        ConvertisseurBaseX::transformChaineBaseXToNombre("coucou",7);
        ConvertisseurBaseX::transformChaineBaseXToNombre("02.38.87.24.90",12);
        ConvertisseurBaseX::transformChaineBaseXToNombre("21380",5);
    }

    public function test_si_chaine_baseX_renvoie_nombres(){
        $this -> assertEquals(ConvertisseurBaseX::transformChaineBaseXToNombre("0",8),0);
        $this -> assertEquals(ConvertisseurBaseX::transformChaineBaseXToNombre("77",8),63);
        $this -> assertEquals(ConvertisseurBaseX::transformChaineBaseXToNombre("143",5),48);
        $this -> assertEquals(ConvertisseurBaseX::transformChaineBaseXToNombre("58",11),63);
        $this -> assertEquals(ConvertisseurBaseX::transformChaineBaseXToNombre("71490",15),358785);
    }
    
}