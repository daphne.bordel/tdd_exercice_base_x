<?php

use PHPUnit\Framework\TestCase;
use App\Utils\ConvertisseurBase10;

class ConvertisseurBase10Test extends TestCase{

    //NUMBER TO CHAINE BASE 10

    public function test_si_nombre_est_un_entier_positif(){
        $this -> expectException(InvalidArgumentException::class);
        ConvertisseurBase10::transformNombreToChaineBase10(-5);
        ConvertisseurBase10::transformNombreToChaineBase10("coucou");
    }

    public function test_si_nombre_renvoie_chaine_en_base10(){
        $this -> assertEquals(ConvertisseurBase10::transformNombreToChaineBase10(0),"0");
        $this -> assertEquals(ConvertisseurBase10::transformNombreToChaineBase10(1),"1");
        $this -> assertEquals(ConvertisseurBase10::transformNombreToChaineBase10(10), "10");
        $this -> assertEquals(ConvertisseurBase10::transformNombreToChaineBase10(101),"101");
        $this -> assertEquals(ConvertisseurBase10::transformNombreToChaineBase10(11011),"11011");
    }


    //CHAINE BASE 10 TO NUMBER

    public function test_si_chaine_est_une_chaine_de_caracteres(){
        $this -> expectException(InvalidArgumentException::class);
        ConvertisseurBase10::transformChaineBase10ToNombre(10101);
    }

    public function test_si_chaine_ne_contient_que_des_nombres_sinon_renvoie_une_erreur(){
        $this -> expectException(InvalidArgumentException::class);
        ConvertisseurBase10::transformChaineBase10ToNombre("coucou");
        ConvertisseurBase10::transformChaineBase10ToNombre("02.38.87.24.90");
    }

    public function test_si_chaine_base10_renvoie_nombre_ecrit_en_base10(){
        $this -> assertEquals(ConvertisseurBase10::transformChaineBase10ToNombre("0"),0);
        $this -> assertEquals(ConvertisseurBase10::transformChaineBase10ToNombre("9"),9);
        $this -> assertEquals(ConvertisseurBase10::transformChaineBase10ToNombre("15"),15);
        $this -> assertEquals(ConvertisseurBase10::transformChaineBase10ToNombre("58"),58);
        $this -> assertEquals(ConvertisseurBase10::transformChaineBase10ToNombre("109"),109);
        $this -> assertEquals(ConvertisseurBase10::transformChaineBase10ToNombre("7490"),7490);
    }
    
}