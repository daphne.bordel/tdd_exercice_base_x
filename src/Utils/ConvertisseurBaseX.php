<?php
namespace App\Utils;

use InvalidArgumentException;

class ConvertisseurBaseX{

    public static function transformNombreToChaineBaseX($nb,$x){
        if (!is_int($nb) || $nb < 0) throw new InvalidArgumentException('Le nombre saisi n\'est pas un entier positif');
        $chaine = "";
        if ($nb == 0) return 0;
        while($nb>0){
            $chaine = strval($nb % $x) . $chaine;
            $nb = intval($nb/$x);
        }
        return $chaine;
    }

     public static function transformChaineBaseXToNombre($chaine,$base){
         if(!is_string($chaine)) throw new InvalidArgumentException("L'élement donné n'est pas une chaine de caractère"); 
         else {
            $nb = intval($chaine);
            $res = 0;
            $indiceExposant = strlen($chaine)-1;
            for ($i=0; $i < strlen($chaine);$i++){
                if (intval(substr($chaine,$i,1)) >=0 && intval(substr($chaine,$i,1)) < $base){
                    $res = $res + intval(substr($nb,$i,1)) * pow($base,$indiceExposant);
                    $indiceExposant--;
                } else {
                    throw new InvalidArgumentException('Le nombre saisi n\'est pas un nombre en base X');
                }
            }
            return $res;
         } 
        
     }
    
}