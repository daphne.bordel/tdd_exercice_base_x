<?php
namespace App\Utils;

use InvalidArgumentException;

class ConvertisseurBase10{

    public static function transformNombreToChaineBase10($nb){
        if (!is_int($nb) || $nb < 0) throw new InvalidArgumentException('Le nombre saisi n\'est pas un entier positif');
        $chaine = "";
        if ($nb == 0) return 0;
        while($nb>0){
            $chaine = strval($nb % 10) . $chaine;
            $nb = intval($nb/10);
        }
        return $chaine;
    }

     public static function transformChaineBase10ToNombre($chaine){
         //vérifie que la chaine saisie est une chaine de caractère
        if(!is_string($chaine)) throw new InvalidArgumentException("L'élement donné n'est pas une chaine de caractère");
        //vérifie si tous les caractères de la chaîne text sont des chiffres.
        if (ctype_digit($chaine)){//chaine = 9
            $longueurChaine = strlen($chaine); //=1
            $res = 0;
            $indiceDernierNombre = $longueurChaine - 1; //0
            $base = 10;
            for($i=0; $i<=$longueurChaine-1;$i++){//test i=1
                $res = $res + pow($base,$i) * substr($chaine,$indiceDernierNombre,1);
                $indiceDernierNombre--;
            }
            return $res;
        } else {
            throw new InvalidArgumentException('Le nombre saisi n\'est pas un binaire');
        }
     }
    
}