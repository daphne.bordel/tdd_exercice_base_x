<?php
namespace App\Utils;

use InvalidArgumentException;

class ConvertisseurBase2{

    public static function transformNombreToChaineBinaire($nb){
        if (!is_int($nb) || $nb < 0) throw new InvalidArgumentException('Le nombre saisi n\'est pas un entier');
        if ($nb == 0) return "0";
        else {
            $res = "";
            while($nb != 0){
                if($nb % 2 == 1){
                    $res = "1" . $res;
                } else {
                    $res = "0" . $res;
                }
                $nb = intdiv($nb,2);
            }
            return $res;
        }
    }

    public static function transformChaineBinaireToNombre($chaine){
        if(!is_string($chaine)) throw new InvalidArgumentException("L'élement donné n'est pas une chaine de caractère");
        else {
            $longueurChaine = strlen($chaine);
            $res = 0;
            $puissance = 0;
            for($i=$longueurChaine-1; $i >= 0; $i--){
                if (intval(substr($chaine,$i,1)) !== 0 && intval(substr($chaine,$i,1)) !== 1){
                    throw new InvalidArgumentException('Le nombre saisi n\'est pas un binaire');
                } else {
                    if(intval(substr($chaine,$i,1)) == 1){
                        $res = $res + pow(2,$puissance);
                    }
                    $puissance++;
                }
            }
            return $res;
        }
    }
    
}